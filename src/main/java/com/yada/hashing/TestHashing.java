/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.hashing;

import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class TestHashing {
    public static void main(String[] args) {
        Hash<String, Integer> h = new Hash<String, Integer>();
        Scanner kb = new Scanner(System.in);
        String finish = kb.next();

        while (!finish.equals("-")) {
            int num = kb.nextInt();
            h.put(finish, num);
            finish = kb.next();
        }
        finish = kb.next();

        if (h.get(finish) == null) {
            System.out.println(h.remove(finish));
            System.out.println("Remove success");
        } else {
            System.out.println(h.get(finish));
        }
    }

}
