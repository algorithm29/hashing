/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.hashing;

/**
 *
 * @author ASUS
 */
public class Hash<Key, Value> {

    private int maps = 100;
    private Value[] vals = (Value[]) new Object[maps];
    private Key[] keys = (Key[]) new Object[maps];

    private int hash(Key key) {
        return (key.hashCode() & 0x7FFFFFFF) % maps;
    }

    public void put(Key key, Value val) {
        int i ;
        for (i = hash(key); keys[i] != null; i = (i + 1) % maps) {
            if (keys[i].equals(key)) {
                break;
            }
        }
        keys[i] = key;
        vals[i] = val;
    }

    public Value get(Key key) {
        for (int i = hash(key); keys[i] != null; i = (i + 1) % maps) {
            if (key.equals(keys[i])) {
                return vals[i];
            }
        }
        return null;
    }
    
    public int remove(Key key) {
        for (int i = hash(key);keys[i] != null; i =(i + 1) % maps) {
             if (key.equals(keys[i])) {
                int temp = (int) keys[i];
                keys[i] = null;
                return temp;
            }
        }
        return 0;
    }
}
